/*
	Syed Ahmed Hussain
	Class: BESE-4B
	Reg #: 05761
	Advanced Programming
	Lab 8 matrix.h 
*/

#include <vector>
#include <chrono>
#include <random>

using namespace std;

class MbyNMatrix{

	public:
		const int size;
		const int row;
		const int col;
		int * vec;
		/*------ Task 3 -------*/
		//int * __restrict vec;

	MbyNMatrix(int x, int y) : row(x), col(y), size(x*y){
		unsigned seed = chrono::system_clock::now().time_since_epoch().count();
		mt19937 generator(seed);
		vec = new int[size];
		for (int i = 0; i < size; i++){
			vec[i] = generator() % 5;
		}
	}
};

vector<int> M_M_mult_val(MbyNMatrix x, MbyNMatrix y);
vector<int> M_M_mult_ref(MbyNMatrix *x, MbyNMatrix *y);