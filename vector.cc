/*
	Syed Ahmed Hussain
	Class: BESE-4B
	Reg #: 05761
	Advanced Programming
	Lab 8 vector.cc 
*/

#include "vector.h"
#include "matrix.h"
 
MbyNMatrix M(1500, 1500);
MbyNMatrix Q(1500, 1);
void *M_M_mult(void* slice){

	long sl = (long)slice;
	vector<int> Ans;
	if (M.col == Q.row){
		int initial = (sl * M.row) / num_threads;
		int limit = ((sl + 1) * M.row) / num_threads;
		int sum;
		for (int i = initial; i < limit; i++){
			for (int j = 0; j < Q.col; j++){
				sum = 0;
				for (int k = 0; k < Q.row; k++){
					sum += M.vec[i + k] * Q.vec[j + k*Q.col];
				}
				Ans.push_back(sum);
			}
		}
	    
	}
	
	return NULL;
}
