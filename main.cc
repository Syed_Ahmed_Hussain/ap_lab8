/*
	Syed Ahmed Hussain
	Class: BESE-4B
	Reg #: 05761
	Advanced Programming
	Lab 8 main.cc 
*/

#include <iostream>
#include <pthread.h>
#include "matrix.h"
#include "vector.h"

int main(){

	pthread_t threads[32];
	//cout << "Enter the number of threads you want to use (2, 4, 16 or 32): ";
	//cin >> num_threads;

	MbyNMatrix M(15, 15);
	MbyNMatrix Q(15, 15);
	
	auto begin = chrono::high_resolution_clock::now();

	/*-------------------------------- Task 1 ----------------------------------
		for (int x = 0; x < num_threads; x++){
			pthread_create(&threads[x], NULL, M_M_mult, (void*)x);
		}

		for (int x = 0; x < num_threads; x++){
			pthread_join(threads[x], NULL);
		}

	*/
	/*-------------------------------- Task 2 ----------------------------------*/

		vector<int> Ans1 = M_M_mult_val(M, Q);

		vector<int> Ans2 = M_M_mult_ref(&M, &Q);

	auto end = chrono::high_resolution_clock::now();
	
	cout << "Duration: "<< chrono::duration_cast <chrono::nanoseconds> (end - begin).count() << " ns" << endl;

	getchar();
	return 0;
}