/*
	Syed Ahmed Hussain
	Class: BESE-4B
	Reg #: 05761
	Advanced Programming
	Lab 8 matrix.cc 
*/

#include "matrix.h"

vector<int> M_M_mult_val(MbyNMatrix x, MbyNMatrix y){
	vector<int> Ans;
	if (x.col == y.row){
		int sum;
		for (int i = 0; i < x.row; i++){
			for (int j = 0; j < y.col; j++){
				sum = 0;
				for (int k = 0; k < y.row; k++){
					sum += x.vec[i + k] * y.vec[j + k*y.col];
				}
				Ans.push_back(sum);
			}
		}
	}
	return Ans;
}

vector<int> M_M_mult_ref(MbyNMatrix *x, MbyNMatrix *y){
	vector<int> Ans;
	if (x->col == y->row){
		int sum;
		for (int i = 0; i < x->row; i++){
			for (int j = 0; j < y->col; j++){
				sum = 0;
				for (int k = 0; k < y->row; k++){
					sum += x->vec[i + k] * (y->vec[j + k*(y->col)]);
				}
				Ans.push_back(sum);
			}
		}
	}
	return Ans;
}